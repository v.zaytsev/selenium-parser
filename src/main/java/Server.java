import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.http.HttpConnectTimeoutException;
import java.net.http.HttpRequest;

import com.sun.net.httpserver.*;
import MyHttpHandler.MyHttpHandler;


public class Server {
    public static void main(String[] args) {
        try {
            HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);
            server.createContext("/database", new MyHttpHandler());
            server.setExecutor(null); // creates a default executor
            server.start();
        }
        catch (Exception e) {
            System.out.println("err");
        }
    }
}