package MyParser;

import MyParser.BasicTypes.News;
import MyParser.Parser;
import MySQL_interactions.interactors;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class vc_parser extends Parser {
    private static News getNews_vc(WebElement post, int sourceID) {
        String id = post.findElement(By.cssSelector("div[data-feed-item-id]")).getAttribute("data-feed-item-id");
        id = cutString(id, 50);
        String txt = post.findElement(By.cssSelector(".content-container")).findElement(By.cssSelector(".l-island-a:not(.content-title)")).getText();
        txt = cutString(txt, 30);
        String title = post.findElement(By.cssSelector(".content-container")).findElement(By.cssSelector(".content-title.l-island-a")).getText();
        title = cutString(title, 100);
        String url = post.findElement(By.cssSelector(".content-link")).getAttribute("href");
        url = cutString(url, 200);
        return new News(id, sourceID, txt, title, url);
    }

    public static int vc_parser(int source_id, String url, Statement statement, ArrayList<News> arr_news) {
        ChromeDriver driver = getADriver();
        driver.get(url);
        int cnt = 0;
        List<WebElement> News = driver.findElements(By.cssSelector((".feed__item")));
        for (int j = 0; j < 5; ++j) {
            News item = getNews_vc(News.get(j), source_id);
            if (news_unique(arr_news, item)) {
                interactors.pushNews(item, statement);
                ++cnt;
            }
        }
        driver.close();
        return cnt;
    }
}
