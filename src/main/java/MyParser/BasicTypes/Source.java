package MyParser.BasicTypes;

public class Source {
    int id;
    String url;
    String txt;
    String md;

    public Source(int Id, String Link, String Txt, String Md) {
        id = Id;
        url = Link;
        txt = Txt;
        md = Md;
    }
    public Source() {
        this(0, "", "", "");
    }

    public int getId() {return this.id;}
    public String getTxt() {return this.txt;}
    public String getURL() {return this.url;}
    public String getMd() {return this.md;}
}
