package MyParser;

import MyParser.BasicTypes.News;
import MySQL_interactions.interactors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class vk_parser extends Parser {
    private static News getNews_vk(WebElement post, int sourceID) {
        String id = post.getAttribute("id");
        id = cutString(id, 50);
        String txt = post.findElement(By.cssSelector(".wall_post_text")).getText(), title;
        title = cutString(txt, 100);
        txt = cutString(txt, 30);
        String url = post.findElement(By.cssSelector(".post_link")).getAttribute("href");
        url = cutString(url, 200);
        return new News(id, sourceID, txt, title, url);
    }

    public static int vk_parser(int source_id, String url, Statement statement, ArrayList<News> arr_news) {
        ChromeDriver driver = getADriver();
        driver.get(url);
        int cnt = 0;
        List<WebElement> News = driver.findElements(By.cssSelector(("div[post_view_hash]")));
        for (int j = 0; j < 5; ++j) {
            News item = getNews_vk(News.get(j), source_id);
            if (news_unique(arr_news, item)) {
                interactors.pushNews(item, statement);
                ++cnt;
            }
        }
        driver.close();
        return cnt;
    }
}
