package CRUD;

import java.awt.*;

public class Notify {

    public static void showNotify(int newsCount) throws AWTException {
        SystemTray tray = SystemTray.getSystemTray();

        //If the icon is a file
        Image image = Toolkit.getDefaultToolkit().createImage("icon.png");
        //Alternative (if the icon is on the classpath):
        //Image image = Toolkit.getDefaultToolkit().createImage(getClass().getResource("icon.png"));

        TrayIcon trayIcon = new TrayIcon(image, "Tray Demo");
        String message = "Найдено " + newsCount + " новых новостей";
        //Let the system resize the image if needed
        trayIcon.setImageAutoSize(true);
        //Set tooltip text for the tray icon
        trayIcon.setToolTip("System tray icon demo");
        tray.add(trayIcon);
        if(newsCount > 0)  trayIcon.displayMessage("Поиск новостей", message, TrayIcon.MessageType.WARNING);
        else trayIcon.displayMessage("Поиск новостей", "Новых новостей не найдено", TrayIcon.MessageType.INFO);

        tray.remove(trayIcon);
    }
}