package MyParser;

import MyParser.BasicTypes.News;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.ArrayList;

public class Parser {

    public static boolean news_unique(ArrayList<News> arr_news, News news) {
        for (News arr_new : arr_news) {
            if (arr_new.getId().equals(news.getId()) && arr_new.getSource_id() == news.getSource_id()) {
                return false;
            }
        }
        return true;
    }

    public static String cutString(String s, int limit)
    {
        if (limit <= 3)
            return s;
        if (s.length() > limit)
            s = s.substring(0, limit - 3) + "...";
        return s;
    }

    public static ChromeDriver getADriver() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        return new ChromeDriver(options);
    }
}
