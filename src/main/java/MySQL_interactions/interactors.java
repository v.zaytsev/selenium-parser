package MySQL_interactions;

import MyParser.BasicTypes.News;
import MyParser.BasicTypes.Source;

import java.sql.Statement;

public class interactors {
    public static void pushNews(News news, Statement statement) {
        try {
            String sqlCommand = "INSERT INTO news (id, sourceid, title, txt, url)\n" +
                    "VALUES('" + news.getId() + "'," + news.getSource_id() + ",'" + news.getTitle() + "','" +
                    news.getTxt() + "', '" + news.getURL() + "')";
            statement.executeUpdate(sqlCommand);
            sqlCommand = "UPDATE checkpoints SET mark = \"" + news.getId() + "\" WHERE id = " + news.getSource_id();
            statement.executeUpdate(sqlCommand);
        } catch (Exception e) {
            System.out.println("Push failed");
        }
    }

    public static void pushSource(Statement st, Source sc) {
        try {
            String sqlCommand = "INSERT INTO sources (url, txt, md)\n" +
                    "VALUES('" + sc.getURL() + "','" + sc.getTxt() + "','" + sc.getMd() + "')";
            st.executeUpdate(sqlCommand);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
