package MyHttpHandler;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.devtools.v85.cachestorage.model.Header;

import java.beans.JavaBean;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import MyJsonHandler.JsonHandler;

public class MyHttpHandler implements HttpHandler{
        @Override
        public void handle(HttpExchange exchange) throws IOException {
            if("GET".equals(exchange.getRequestMethod())) {
                String all_sources = JsonHandler.serverAsks_forAllSources();
                if (exchange.getRequestURI().toString().split("/").length >= 3){
                    String sources = JsonHandler.serverAsks_forSourcesByMd(exchange.getRequestURI().toString().split("/")[2]);
                    if (exchange.getRequestURI().toString().split("/").length >= 4) {
                        String particular_sources = JsonHandler.serverAsks_forSourcesById(
                                exchange.getRequestURI().toString().split("/")[2],
                                exchange.getRequestURI().toString().split("/")[3]
                        );
                        if (particular_sources != null) {
                            if (exchange.getRequestURI().toString().split("/").length >= 5
                                    && "news".equals(exchange.getRequestURI().toString().split("/")[4])) {
                                JsonParser jsonParser = new JsonParser();
                                JsonObject jo = (JsonObject) jsonParser.parse(particular_sources);
                                String news = JsonHandler.serverAsks_forNews(jo.get("id").getAsInt());
                                exchange.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
                                exchange.sendResponseHeaders(200, news.getBytes().length);
                                OutputStream os = exchange.getResponseBody();
                                os.write(news.getBytes());
                                os.flush();
                                os.close();
                                exchange.close();
                            }

                            exchange.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
                            exchange.sendResponseHeaders(200, particular_sources.getBytes().length);
                            OutputStream os = exchange.getResponseBody();
                            os.write(particular_sources.getBytes());
                            os.flush();
                            os.close();
                            exchange.close();
                        }
                    }

                    exchange.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
                    exchange.sendResponseHeaders(200, sources.getBytes().length);
                    OutputStream os = exchange.getResponseBody();
                    os.write(sources.getBytes());
                    os.flush();
                    os.close();
                    exchange.close();
                }
                exchange.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
                exchange.sendResponseHeaders(200, all_sources.getBytes().length);
                OutputStream os = exchange.getResponseBody();
                os.write(all_sources.getBytes());
                os.flush();
                os.close();
                exchange.close();
            }
            else if("POST".equals(exchange.getRequestMethod())) {
                InputStream is = exchange.getRequestBody();
                String json = CharStreams.toString(new InputStreamReader(
                        is, Charsets.UTF_8));
                JsonHandler.serverAdds_newSource(json);
            }
            else if("PUT".equals(exchange.getRequestMethod())) {

            }
            else if("DELETE".equals(exchange.getRequestMethod())) {
                System.out.println("delete");
                InputStream is = exchange.getRequestBody();
                String json = CharStreams.toString(new InputStreamReader(
                        is, Charsets.UTF_8));
                System.out.println(json);
                try{
                    JSONObject obj = (JSONObject)(new JSONParser()).parse(json);
                    JsonHandler.serverDeletesSource(obj.get("url").toString());
                }
                catch (Exception e){
                    System.out.println(e);
                }
            }
        }
}
