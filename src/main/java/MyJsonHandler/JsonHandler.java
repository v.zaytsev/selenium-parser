package MyJsonHandler;

import MyParser.BasicTypes.Source;
import com.google.gson.*;
import MyParser.BasicTypes.News;
import java.util.ArrayList;
import CRUD.Console;

public class JsonHandler {
    //The server asks for a json file with all news from a site
    public static String serverAsks_forNews(int sourceID) {
        Source sc = Console.getSource(sourceID);
        Console.parse(sc);
        ArrayList<News> lst = Console.GetNews(sourceID);
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
        return gson.toJson(lst);
    }

    public static String serverAsks_forAllSources() {
        ArrayList<Source> lst = Console.GetAllSources();
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
        return gson.toJson(lst);
    }

    public static String serverAsks_forSourcesByMd(String md) {
        ArrayList<Source> lst = Console.GetSources(md);
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
        return gson.toJson(lst);
    }

    public static String serverAsks_forSourcesById(String md, String id) {
        ArrayList<Source> lst = Console.GetSources(md);
        for(int i = 0; i < lst.size(); ++i){
            if (String.valueOf(lst.get(i).getId()).equals(id)){
                Gson gson = new GsonBuilder()
                        .setPrettyPrinting()
                        .create();
                return gson.toJson(lst.get(i));
            }
        }
        return null;
    }

    public static void serverAdds_newSource(String json) {
        Source sc = new Gson().fromJson(json, Source.class);
        Console.PushSource(sc.getMd(), sc.getURL(), sc.getTxt());
    }

    public static void serverDeletesSource(String url) {
        Console.DeleteSource(url);
    }
}
