package MyParser.BasicTypes;

import java.sql.Statement;

public class News {
    String id;
    int source_id;
    String txt;
    String title;
    String url;

    public News(String Id, int Source_id, String Txt, String Title, String Link) {
        id = Id;
        source_id = Source_id;
        txt = Txt;
        title = Title;
        url = Link;

    }

    public String getId() {return this.id;}
    public int getSource_id() {return this.source_id;}
    public String getTxt() {return this.txt;}
    public String getTitle() {return this.title;}
    public String getURL() {return this.url;}
}
