package MyParser;

import MyParser.BasicTypes.News;
import MySQL_interactions.interactors;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.sql.Statement;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class redditParser extends Parser {

    public static int redditParse(final int source_id, final String url, Statement st, ArrayList<News> existingNews) {
        int res = 0;
        ChromeDriver driver =  getADriver();
        driver.get(url);

        List<WebElement> posts = driver.findElements(By.cssSelector(("div[class^=_1oQyIsiPH]:not([id^=t3_z])")));
        for (int i = 0; (i < posts.size()) && (i < 5); i++) {
            WebElement currentPost = posts.get(i);
            News item = getNews(currentPost, source_id);
            if (news_unique(existingNews, item)) {
                interactors.pushNews(item, st);
                res++;
            }
        }

        driver.close();

        return res;
    }

    private static News getNews(WebElement news, int source_id) {
        String id = news.getAttribute("id");
        id = cutString(id, 50);

        String url = news.findElement(By.cssSelector("div[class^=_3-miAEojrCvx_4FQ8x3P-s]")).findElement(By.cssSelector("a")).getAttribute("href");
        url = cutString(url, 200);

        String title = extractedPostTitle(news);

        for (int i = 0; i < title.length(); i++)
            if (title.charAt(i) == '\'') {
                title = title.substring(0, i) + "\\" + title.substring(i);
                i++;
            }
        String txt = "LOOK AT THE TITLE";

        return new News(id, source_id, txt, title, url);
    }

    private static String extractedPostTitle(WebElement news) {
        String postTitle = news.findElement(By.cssSelector("h3")).getText();
        if (postTitle.length() > 100) {
            postTitle = postTitle.substring(0, 97) + "...";
        }
        return postTitle;
    }
}