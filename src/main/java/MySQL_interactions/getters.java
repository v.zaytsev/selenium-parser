package MySQL_interactions;

import MyParser.BasicTypes.*;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class getters {
    public static ArrayList<Source> get_sources(Statement statement) {
        try {
            ArrayList<Source> dataSources = new ArrayList<>();
            String sqlCommand = "SELECT * FROM sources";
            ResultSet raw = statement.executeQuery(sqlCommand);
            int i = 0;
            while (raw.next()) {
                Source elem = new Source(Integer.parseInt(raw.getString("id")), raw.getString("url"),
                        raw.getString("txt"), raw.getString("md"));
                dataSources.add(i, elem);
                i += 1;
            }

            return dataSources;
        } catch (Exception e) {
            System.out.println("Get sources failed");
            return new ArrayList<>();
        }
    }
    public static ArrayList<Source> get_sources(Statement statement, String md) {
        try {
            ArrayList<Source> dataSources = new ArrayList<>();
            String sqlCommand = "SELECT * FROM sources";
            ResultSet raw = statement.executeQuery(sqlCommand);
            int i = 0;
            while (raw.next()) {
                String curMD = raw.getString("md");
                if (!curMD.equals(md))
                    continue;
                Source elem = new Source(Integer.parseInt(raw.getString("id")), raw.getString("url"),
                        raw.getString("txt"), raw.getString("md"));
                dataSources.add(i, elem);
                i += 1;
            }

            return dataSources;
        } catch (Exception e) {
            System.out.println("Get sources failed");
            return new ArrayList<>();
        }
    }

    public static ArrayList<News> get_news(Statement statement) {
        try {
            ArrayList<News> news = new ArrayList<>();
            String sqlCommand = "SELECT * FROM news";
            ResultSet s = statement.executeQuery(sqlCommand);
            int q = 0;
            while (s.next()) {
                News elem = new News(s.getString("id"),
                        Integer.parseInt(s.getString("sourceid")), s.getString("txt"),
                        s.getString("title"), s.getString("url"));
                news.add(q, elem);
                ++q;
            }
            return news;
        } catch (Exception e) {
            System.out.println("Get news failed");
            return new ArrayList<>();
        }
    }

    public static ArrayList<News> get_news(Statement statement, int SourceID) {
        try {
            ArrayList<News> news = new ArrayList<>();
            String sqlCommand = "SELECT * FROM news";
            ResultSet s = statement.executeQuery(sqlCommand);
            int q = 0;
            while (s.next()) {
                int curID = s.getInt("sourceid");
                if (curID != SourceID){
                    continue;
                }
                News elem = new News(s.getString("id"),
                        Integer.parseInt(s.getString("sourceid")), s.getString("txt"),
                        s.getString("title"), s.getString("url"));
                news.add(q, elem);
                ++q;
            }
            return news;
        } catch (Exception e) {
            System.out.println("Get news failed");
            return new ArrayList<>();
        }
    }
}
