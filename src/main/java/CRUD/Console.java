package CRUD;

import MyParser.BasicTypes.*;
import MyParser.*;
import MySQL_interactions.*;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

public class Console {
    public static void displayInfo(Source s) {
        System.out.printf("%d.URL:%40s \tTxt:%30s \tMd:%10s\n", s.getId(), s.getURL(), s.getTxt(), s.getMd());
    }

    public static void displayInfo(News n) {
        System.out.printf("URL:%s\nTitle:\t%s\nTxt:\t%s\n\n", n.getURL(), n.getTitle(), n.getTxt());
    }

    public static void showSources(ArrayList<Source> sources) {
        for (Source elem : sources) {
            displayInfo(elem);
        }
    }

    public static void showNews(ArrayList<News> news) {
        for (News elem : news) {
            displayInfo(elem);
        }
    }

    private static Statement connect_getStatement()
            throws ClassNotFoundException, InstantiationException,
            NoSuchMethodException, SecurityException,
            IllegalAccessException, IllegalArgumentException, InvocationTargetException,
            java.sql.SQLException
    {
        Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
        String databasePath = "jdbc:mysql://localhost/sources", databaseUsername = "root", databasePassword = "wcRhioi";
        Connection con = DriverManager.getConnection(databasePath, databaseUsername, databasePassword);
        return con.createStatement();
    }

    public static void console() {

        Scanner in = new Scanner(System.in);

        try {
            Statement st = connect_getStatement();

            System.out.println("What do I parse?\n0.Show the news\n");

            ArrayList<Source> sources = getters.get_sources(st);
            showSources(sources);

            ArrayList<News> news = getters.get_news(st);
            int choice = in.nextInt();
            if (choice == 0) {
                showNews(news);
                return;
            }

            Source sc = sources.get(choice - 1);

            String url = sc.getURL();
            String md = sc.getMd();
            int id = sc.getId();
            int n = 0;

            switch (md) {
                case "vk_groups": {
                    n = vk_parser.vk_parser(id, url, st, news);
                    break;
                }
                case "vc_tapes": {
                    n = vc_parser.vc_parser(id, url, st, news);
                    break;
                }
                case "rd_tech": {
                    n = redditParser.redditParse(id, url, st, news);
                    break;
                }
                default:
                    n = 0;
            }

            try {
                Notify.showNotify(n);
            } catch (Exception e) {
                System.out.println("Failed notification");
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (InvocationTargetException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (NoSuchMethodException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        }
    }

    public static void parse(Source sc) {
        try {
            Statement st = connect_getStatement();
            ArrayList<News> news = getters.get_news(st);
            String url = sc.getURL();
            String md = sc.getMd();
            int id = sc.getId();

            if (md.equals("vk_groups")) {
                vk_parser.vk_parser(id, url, st, news);
            } else if (md.equals("vc_tapes")) {
                vc_parser.vc_parser(id, url, st, news);
            } else if (md.equals("rd_tech")) {
                redditParser.redditParse(id, url, st, news);
            }
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (InvocationTargetException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (NoSuchMethodException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        }
    }

    public static ArrayList<News> GetNews(int ID) {
        try {
            Statement st = connect_getStatement();
            return getters.get_news(st, ID);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (InvocationTargetException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (NoSuchMethodException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        }
        return new ArrayList<>();
    }
    public static ArrayList<Source> GetAllSources() {
        try {
            Statement st = connect_getStatement();
            return getters.get_sources(st);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (InvocationTargetException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (NoSuchMethodException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        }
        return new ArrayList<>();
    }

    public static ArrayList<Source> GetSources(String md) {
        try {
            Statement st = connect_getStatement();
            return getters.get_sources(st, md);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (InvocationTargetException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (NoSuchMethodException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        }
        return new ArrayList<>();
    }

    public static Source getSource(String URL) {
        try {
            Statement st = connect_getStatement();
            ArrayList<Source> scs = getters.get_sources(st);
            for (Source sc : scs) {
                if (sc.getURL().equals(URL))
                    return sc;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return new Source();
    }

    public static Source getSource(int sourceID) {
        try {
            Statement st = connect_getStatement();
            ArrayList<Source> scs = getters.get_sources(st);
            for (Source sc : scs) {
                if (sc.getId() == sourceID)
                    return sc;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return new Source();
    }


    public static void PushSource(String md, String URL, String name) {
        try {
            Statement st = connect_getStatement();
            Source sc = new Source(0, URL, name, md);
            interactors.pushSource(st, sc);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void DeleteSource(String url) {
        String request = "DELETE FROM sources WHERE url=" + url + ";";
        try {
            Statement st = connect_getStatement();
            st.executeUpdate(request);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

}
